import argparse
import os
import os.path
import subprocess
import tempfile

import pystache

DEFAULT_TEMPLATE = 'template.html.mustache'
CHROME = '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'


def fill_template(images, template_path):
    image_map = {'images': [{'fname': fname} for fname in images]}
    with open(template_path) as template_fobj:
        template = template_fobj.read()
    return pystache.render(template, image_map)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('images', nargs='+', help="Images to cast")
    parser.add_argument(
        '--template',
        default=DEFAULT_TEMPLATE,
        help="HTML template to cast images with",
    )
    args = parser.parse_args()

    html_contents = fill_template(args.images, args.template)

    with tempfile.NamedTemporaryFile(suffix='.html') as cast_file:
        cast_file.write(html_contents)
        cast_file.flush()
        with open(os.devnull, 'w') as devnull:
            subprocess.check_call([CHROME, cast_file.name], stderr=devnull)
